For Ubuntu Planet module to allow Administrators to have users feeds to be displayed 
in Planet style.



[Install Process]

1. Copy the Ubuntu-Planet module directory into your Drupal modules directory.

2. Go to Admin/Modules and enable the Ubuntu-Planet module.

3. To change the configuration and to add feeds go to admin/settings/ubuntu_planet.


